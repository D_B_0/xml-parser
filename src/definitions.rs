#[derive(Debug, PartialEq)]
pub struct Document {
    pub prolog: Prolog,
    pub root: Element, // :()
}

#[derive(Debug, PartialEq)]
pub struct Element {
    pub name: Name,
    pub attributes: Vec<Attribute>,
    pub content: Vec<Content>,
}

#[derive(Debug, PartialEq)]
pub enum Content {
    Element(Element),
    Reference(String),
    CData(String),
    PrcessingIinstrucion(ProcessingInstruction),
    Comment(String),
    CharData(String),
}

#[derive(Debug, PartialEq)]
pub struct Attribute {
    pub name: Name,
    pub value: AttributeValue,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ProcessingInstruction {
    pub target: Name,
    pub content: String,
}

#[derive(Debug, Clone, PartialEq)]
pub struct XMLDeclaration {
    pub version: String,
    pub encoding: Option<String>,
    pub standalone: Option<bool>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Doctype {
    pub name: Name,
    // TODO: this is not the final type
    pub external_id: Option<ExternalID>,
    // TODO: this is not the final type
    pub int_subset: Option<Vec<MarkupDeclaration>>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum ExternalID {
    System {
        identifier: SystemLiteral,
    },
    Public {
        pub_literal: PubIDLiteral,
        identifier: SystemLiteral,
    },
}

#[derive(Debug, Clone, PartialEq)]
pub enum Mixed {
    PCData,
    Name(Name),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Choice(pub(crate) Vec<ContentParticle>);

#[derive(Debug, Clone, PartialEq)]
pub struct Sequence(pub(crate) Vec<ContentParticle>);

#[derive(Debug, Clone, PartialEq)]
pub enum ContentParticle {
    One(NameOrChoiceOrSequence),
    Optional(NameOrChoiceOrSequence),
    Many0(NameOrChoiceOrSequence),
    Many1(NameOrChoiceOrSequence),
}

#[derive(Debug, Clone, PartialEq)]
pub enum NameOrChoiceOrSequence {
    Name(Name),
    Choice(Choice),
    Sequence(Sequence),
}

#[derive(Debug, Clone, PartialEq)]
pub enum ChoiceOrSequence {
    Choice(Choice),
    Sequence(Sequence),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Children {
    One(ChoiceOrSequence),
    Optional(ChoiceOrSequence),
    Many0(ChoiceOrSequence),
    Many1(ChoiceOrSequence),
}

#[derive(Debug, Clone, PartialEq)]
pub enum ContentSpec {
    Any,
    Empty,
    Mixed(Vec<Mixed>),
    Children(Children),
}

#[derive(Debug, Clone, PartialEq)]
pub struct ElementDeclaration {
    pub name: Name,
    pub content_spec: ContentSpec,
}

#[derive(Debug, Clone, PartialEq)]
pub enum TokenizedType {
    Id,
    IdRef,
    IdRefs,
    Entity,
    Entities,
    NMToken,
    NMTokens,
}

#[derive(Debug, Clone, PartialEq)]
pub enum EnumeratedType {
    NotationType(Vec<Name>),
    Enumeration(Vec<NameToken>),
}

#[derive(Debug, Clone, PartialEq)]
pub enum AttributeType {
    StringType,
    TokenizedType(TokenizedType),
    EnumeratedType(EnumeratedType),
}

#[derive(Debug, Clone, PartialEq)]
pub enum DefaultDeclaration {
    Required,
    Implied,
    Fixed(AttributeValue),
}

#[derive(Debug, Clone, PartialEq)]
pub struct AttributeDefinition {
    pub name: Name,
    pub attribute_type: AttributeType,
    pub default_declaration: DefaultDeclaration,
}

#[derive(Debug, Clone, PartialEq)]
pub struct AttributeListDeclaration {
    pub name: Name,
    pub attribute_definitions: Vec<AttributeDefinition>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum EntityDef {
    EntityValue(EntityValue),
    External(ExternalID, Option<NDataDeclaration>),
}

#[derive(Debug, Clone, PartialEq)]
pub enum PEDef {
    EntityValue(EntityValue),
    External(ExternalID),
}

#[derive(Debug, Clone, PartialEq)]
pub struct NDataDeclaration(pub Name);

#[derive(Debug, Clone, PartialEq)]
pub struct EntityValue(pub String);

#[derive(Debug, Clone, PartialEq)]
pub struct GEDeclaration {
    pub name: Name,
    pub entity_def: EntityDef,
}

#[derive(Debug, Clone, PartialEq)]
pub struct PEDeclaration {
    pub name: Name,
    pub pe_def: PEDef,
}

#[derive(Debug, Clone, PartialEq)]
pub enum EntityDeclaration {
    GEDeclaration(GEDeclaration),
    PEDeclaration(PEDeclaration),
}

#[derive(Debug, Clone, PartialEq)]
pub struct PublicID(pub PubIDLiteral);

#[derive(Debug, Clone, PartialEq)]
pub enum Id {
    ExternalID(ExternalID),
    PublicID(PublicID),
}

#[derive(Debug, Clone, PartialEq)]
pub struct NotationDeclaration {
    pub name: Name,
    pub id: Id,
}

#[derive(Debug, Clone, PartialEq)]
pub enum MarkupDeclaration {
    ElementDeclaration(ElementDeclaration),
    AttributeListDeclaration(AttributeListDeclaration),
    EntityDeclaration(EntityDeclaration),
    NotationDeclaration(NotationDeclaration),
    ProcessingInstruction(ProcessingInstruction),
    Comment(String),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Prolog {
    pub xml_declaration: Option<XMLDeclaration>,
    pub doctype: Option<Doctype>,
}

#[derive(PartialEq, Debug, Clone)]
pub struct Name(pub(crate) String);

#[derive(PartialEq, Debug, Clone)]
pub struct NameToken(pub(crate) String);

#[derive(PartialEq, Debug, Clone)]
pub struct AttributeValue(pub(crate) String);

#[derive(PartialEq, Debug, Clone)]
pub struct SystemLiteral(pub(crate) String);

#[derive(PartialEq, Debug, Clone)]
pub struct PubIDLiteral(pub(crate) String);

pub enum Misc {
    Comment(String),
    ProcessingInstruction(ProcessingInstruction),
    Space,
}
