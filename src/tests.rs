use crate::{definitions::*, parser::*};

#[test]
fn name() {
    assert_eq!(parse_name("source"), Ok(("", Name("source".to_owned()))));
    assert_eq!(parse_name("name"), Ok(("", Name("name".to_owned()))));
    assert_eq!(parse_name("n"), Ok(("", Name("n".to_owned()))));
}

#[test]
fn element() {
    assert_eq!(
        parse_element("<name></name>"),
        Ok((
            "",
            Element {
                name: Name("name".to_owned()),
                attributes: vec!(),
                content: vec!()
            }
        ))
    );
    assert_eq!(
        parse_element("<name att_name=\"attribute value\"></name>"),
        Ok((
            "",
            Element {
                name: Name("name".to_owned()),
                attributes: vec![Attribute {
                    name: Name("att_name".to_owned()),
                    value: AttributeValue("attribute value".to_owned())
                }],
                content: vec!()
            }
        ))
    );
    assert_eq!(
        parse_element("<name att_name=\"attribute value\" />"),
        Ok((
            "",
            Element {
                name: Name("name".to_owned()),
                attributes: vec![Attribute {
                    name: Name("att_name".to_owned()),
                    value: AttributeValue("attribute value".to_owned())
                }],
                content: vec!()
            }
        ))
    );
    assert_eq!(
        parse_element(
            "<note><to>Tove</to><from>Jani</from><heading>Reminder</heading><body>Don't forget me this weekend!</body></note>"
        ),
        Ok((
            "",
            Element {
                name: Name("note".to_owned()),
                attributes: vec![],
                content: vec![
                    Content::Element(Element {
                        name: Name("to".to_string()),
                        attributes: vec![],
                        content: vec![Content::CharData("Tove".to_string()),],
                    },),
                    Content::Element(Element {
                        name: Name("from".to_string()),
                        attributes: vec![],
                        content: vec![Content::CharData("Jani".to_string()),],
                    },),
                    Content::Element(Element {
                        name: Name("heading".to_string()),
                        attributes: vec![],
                        content: vec![Content::CharData("Reminder".to_string()),],
                    },),
                    Content::Element(Element {
                        name: Name("body".to_string()),
                        attributes: vec![],
                        content: vec![Content::CharData(
                            "Don't forget me this weekend!".to_string()
                        ),],
                    },),
                ],
            },
        ))
    );
}

#[test]
fn xml_declaration() {
    assert_eq!(
        parse_xml_declaration("<?xml version=\"1.0\" standalone='yes'?>"),
        Ok((
            "",
            XMLDeclaration {
                version: "1.0".to_string(),
                encoding: None,
                standalone: Some(true)
            }
        )),
    );
}

#[test]
fn doctype() {
    assert_eq!(
        parse_doctype("<!DOCTYPE note SYSTEM \"Note.dtd\">"),
        Ok((
            "",
            Doctype {
                name: Name("note".to_owned()),
                external_id: Some(ExternalID::System {
                    identifier: SystemLiteral("Note.dtd".to_owned())
                }),
                int_subset: None
            }
        ))
    );
    assert_eq!(
        parse_doctype(
            "<!DOCTYPE note [
<!ENTITY nbsp \"&#xA0;\">
<!ENTITY writer \"Writer: Donald Duck.\">
<!ENTITY copyright \"Copyright: W3Schools.\">
]>"
        ),
        Ok((
            "", 
            Doctype { 
                name: Name("note".to_owned()), 
                external_id: None, 
                int_subset: Some(vec![
                    MarkupDeclaration::EntityDeclaration(EntityDeclaration::GEDeclaration(GEDeclaration { 
                        name: Name("nbsp".to_string()), 
                        entity_def: EntityDef::EntityValue(EntityValue("&#xA0;".to_string())) 
                    })), 
                    MarkupDeclaration::EntityDeclaration(EntityDeclaration::GEDeclaration(GEDeclaration { 
                        name: Name("writer".to_string()), 
                        entity_def: EntityDef::EntityValue(EntityValue("Writer: Donald Duck.".to_string())) 
                    })), 
                    MarkupDeclaration::EntityDeclaration(EntityDeclaration::GEDeclaration(GEDeclaration { 
                        name: Name("copyright".to_string()), 
                        entity_def: EntityDef::EntityValue(EntityValue("Copyright: W3Schools.".to_string())) 
                    }))
                ]) 
            }
        ))
    );
}

#[test]
fn element_declaration() {
    assert_eq!(
        parse_element_declaration("<!ELEMENT note ANY>"),
        Ok((
            "",
            ElementDeclaration {
                name: Name("note".to_owned()),
                content_spec: ContentSpec::Any,
            }
        ))
    );
    assert_eq!(
        parse_element_declaration("<!ELEMENT note EMPTY    >"),
        Ok((
            "",
            ElementDeclaration {
                name: Name("note".to_owned()),
                content_spec: ContentSpec::Empty,
            }
        ))
    );
    assert_eq!(
        parse_element_declaration("<!ELEMENT p (#PCDATA|emph)* >"),
        Ok((
            "",
            ElementDeclaration {
                name: Name("p".to_string()),
                content_spec: ContentSpec::Mixed(vec![
                    Mixed::PCData,
                    Mixed::Name(Name("emph".to_string()))
                ])
            }
        ))
    );
    println!(
        "{:?}",
        parse_element_declaration("<!ELEMENT %name.para; %content.para; >")
    );
}

#[test]
fn mixed() {
    assert_eq!(
        parse_content_spec_mixed("(#PCDATA)"),
        Ok(("", vec![Mixed::PCData]))
    );
    assert_eq!(
        parse_content_spec_mixed("(#PCDATA)*"),
        Ok(("", vec![Mixed::PCData]))
    );
    assert_eq!(
        parse_content_spec_mixed("(#PCDATA | name1 | name2)*"),
        Ok((
            "",
            vec![
                Mixed::PCData,
                Mixed::Name(Name("name1".to_owned())),
                Mixed::Name(Name("name2".to_owned())),
            ]
        ))
    );
}

#[test]
fn content_spec_children() {
    assert_eq!(
        parse_content_spec_children("(name1|name2)"),
        Ok((
            "",
            Children::One(ChoiceOrSequence::Choice(Choice(vec![
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("name1".to_owned()))),
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("name2".to_owned()))),
            ])))
        ))
    );
    assert_eq!(
        parse_content_spec_children("(name1|name2)+"),
        Ok((
            "",
            Children::Many1(ChoiceOrSequence::Choice(Choice(vec![
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("name1".to_owned()))),
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("name2".to_owned()))),
            ])))
        ))
    );
    assert_eq!(
        parse_content_spec_children("(name1,(name2|name3))+"),
        Ok((
            "",
            Children::Many1(ChoiceOrSequence::Sequence(Sequence(vec![
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("name1".to_owned()))),
                ContentParticle::One(NameOrChoiceOrSequence::Choice(Choice(vec![
                    ContentParticle::One(NameOrChoiceOrSequence::Name(Name("name2".to_owned()))),
                    ContentParticle::One(NameOrChoiceOrSequence::Name(Name("name3".to_owned()))),
                ],))),
            ])))
        ))
    );
    assert_eq!(
        parse_content_spec_children("(head, (p | list | note)*, div2*)"),
        Ok((
            "",
            Children::One(ChoiceOrSequence::Sequence(Sequence(vec![
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("head".to_owned()))),
                ContentParticle::Many0(NameOrChoiceOrSequence::Choice(Choice(vec![
                    ContentParticle::One(NameOrChoiceOrSequence::Name(Name("p".to_owned()))),
                    ContentParticle::One(NameOrChoiceOrSequence::Name(Name("list".to_owned()))),
                    ContentParticle::One(NameOrChoiceOrSequence::Name(Name("note".to_owned()))),
                ],))),
                ContentParticle::Many0(NameOrChoiceOrSequence::Name(Name("div2".to_owned()))),
            ])))
        ))
    );
}

#[test]
fn choice() {
    assert_eq!(
        parse_content_spec_children_choice("(p | list | note)"),
        Ok((
            "",
            Choice(vec![
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("p".to_string()))),
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("list".to_string()))),
                ContentParticle::One(NameOrChoiceOrSequence::Name(Name("note".to_string())))
            ])
        )),
    );
}

#[test]
fn entity_declaration() {
    assert_eq!(
        parse_entity_declaration(
            "<!ENTITY Pub-Status \"This is a pre-release of the specification.\">"
        ),
        Ok((
            "",
            EntityDeclaration::GEDeclaration(GEDeclaration {
                name: Name("Pub-Status".to_string()),
                entity_def: EntityDef::EntityValue(EntityValue(
                    "This is a pre-release of the specification.".to_string()
                ))
            })
        ))
    );
    assert_eq!(
        parse_entity_declaration(
            "<!ENTITY open-hatch SYSTEM \"http://www.textuality.com/boilerplate/OpenHatch.xml\">"
        ),
        Ok((
            "",
            EntityDeclaration::GEDeclaration(GEDeclaration {
                name: Name("open-hatch".to_string()),
                entity_def: EntityDef::External(
                    ExternalID::System {
                        identifier: SystemLiteral(
                            "http://www.textuality.com/boilerplate/OpenHatch.xml".to_string()
                        )
                    },
                    None
                )
            })
        ))
    );
    assert_eq!(
        parse_entity_declaration(
            "<!ENTITY hatch-pic SYSTEM \"../grafix/OpenHatch.gif\" NDATA gif >"
        ),
        Ok((
            "",
            EntityDeclaration::GEDeclaration(GEDeclaration {
                name: Name("hatch-pic".to_string()),
                entity_def: EntityDef::External(
                    ExternalID::System {
                        identifier: SystemLiteral("../grafix/OpenHatch.gif".to_string())
                    },
                    Some(NDataDeclaration(Name("gif".to_owned())))
                )
            })
        ))
    );
    assert_eq!(
        parse_entity_declaration("<!ENTITY % pub \"&#xc9;ditions Gallimard\" >"),
        Ok((
            "",
            EntityDeclaration::PEDeclaration(PEDeclaration {
                name: Name("pub".to_string()),
                pe_def: PEDef::EntityValue(EntityValue("&#xc9;ditions Gallimard".to_string()))
            })
        ))
    );
    assert_eq!(
        parse_entity_declaration("<!ENTITY % YN '\"Yes\"' >"),
        Ok((
            "",
            EntityDeclaration::PEDeclaration(PEDeclaration {
                name: Name("YN".to_string()),
                pe_def: PEDef::EntityValue(EntityValue("\"Yes\"".to_string()))
            })
        ))
    );
}

#[test]
fn attribute_list() {
    assert_eq!(
        parse_attribute_list_declaration(
            "<!ATTLIST  termdef
                        id      ID      #REQUIRED
                        name    CDATA   #IMPLIED>"
        ),
        Ok((
            "",
            AttributeListDeclaration {
                name: Name("termdef".to_string()),
                attribute_definitions: vec![
                    AttributeDefinition {
                        name: Name("id".to_string()),
                        attribute_type: AttributeType::TokenizedType(TokenizedType::Id),
                        default_declaration: DefaultDeclaration::Required
                    },
                    AttributeDefinition {
                        name: Name("name".to_string()),
                        attribute_type: AttributeType::StringType,
                        default_declaration: DefaultDeclaration::Implied
                    }
                ]
            }
        ))
    );
    assert_eq!(
        parse_attribute_list_declaration(
            "<!ATTLIST  form
                        method  CDATA   #FIXED \"POST\">"
        ),
        Ok((
            "",
            AttributeListDeclaration {
                name: Name("form".to_string()),
                attribute_definitions: vec![AttributeDefinition {
                    name: Name("method".to_string()),
                    attribute_type: AttributeType::StringType,
                    default_declaration: DefaultDeclaration::Fixed(AttributeValue(
                        "POST".to_owned()
                    ))
                }]
            }
        ))
    );
    assert_eq!(
        parse_attribute_list_declaration(
            "<!ATTLIST list
    type    (bullets|ordered|glossary)  \"ordered\">"
        ),
        Ok((
            "",
            AttributeListDeclaration {
                name: Name("list".to_string()),
                attribute_definitions: vec![AttributeDefinition {
                    name: Name("type".to_string()),
                    attribute_type: AttributeType::EnumeratedType(EnumeratedType::Enumeration(
                        vec![
                            NameToken("bullets".to_string()),
                            NameToken("ordered".to_string()),
                            NameToken("glossary".to_string())
                        ]
                    )),
                    default_declaration: DefaultDeclaration::Fixed(AttributeValue(
                        "ordered".to_string()
                    ))
                }]
            }
        ))
    );
}

#[test]
fn default_declaration() {
    assert_eq!(
        parse_default_declaration("#IMPLIED"),
        Ok(("", DefaultDeclaration::Implied))
    );
    assert_eq!(
        parse_default_declaration("#REQUIRED"),
        Ok(("", DefaultDeclaration::Required))
    );
    assert_eq!(
        parse_default_declaration("#FIXED \"POST\""),
        Ok((
            "",
            DefaultDeclaration::Fixed(AttributeValue("POST".to_owned()))
        ))
    );
    assert_eq!(
        parse_default_declaration("\"ordered\""),
        Ok((
            "",
            DefaultDeclaration::Fixed(AttributeValue("ordered".to_owned()))
        ))
    );
}

#[test]
fn enumerated_type() {
    assert_eq!(
        parse_enumerated_type("(bullets|ordered|glossary)"),
        Ok((
            "",
            EnumeratedType::Enumeration(vec![
                NameToken("bullets".to_string()),
                NameToken("ordered".to_string()),
                NameToken("glossary".to_string())
            ])
        ))
    );
    assert_eq!(
        parse_enumerated_type("NOTATION (bullets|ordered|glossary)"),
        Ok((
            "",
            EnumeratedType::NotationType(vec![
                Name("bullets".to_string()),
                Name("ordered".to_string()),
                Name("glossary".to_string())
            ])
        ))
    );
}

#[test]
fn notation() {
    assert_eq!(
        parse_notation_declaration("<!NOTATION gif SYSTEM \"gif\" >"),
        Ok((
            "",
            NotationDeclaration {
                name: Name("gif".to_string()),
                id: Id::ExternalID(ExternalID::System {
                    identifier: SystemLiteral("gif".to_string())
                })
            }
        )),
    );
    assert_eq!(
        parse_notation_declaration(
            "<!NOTATION gif89a PUBLIC \"-//CompuServe//NOTATION Graphics Interchange Format 89a//EN\" \"gif\">"
        ),
        Ok((
            "", 
            NotationDeclaration { 
                name: Name("gif89a".to_string()), 
                id: Id::ExternalID(
                    ExternalID::Public { 
                        pub_literal: PubIDLiteral(
                            "-//CompuServe//NOTATION Graphics Interchange Format 89a//EN".to_string()
                        ), 
                        identifier: SystemLiteral("gif".to_string())
                    }
                ) 
            }
        ))
    );
}

#[test]
fn scratch() {}
