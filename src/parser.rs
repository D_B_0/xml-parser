use crate::definitions::*;
use nom::{
    branch::alt,
    bytes::complete::{is_a, is_not, tag, take_while, take_while1},
    character::complete::{char, digit1, none_of, one_of},
    combinator::{eof, map, opt, peek, recognize, success, verify},
    multi::{many0, many_till, separated_list1},
    sequence::{delimited, pair, preceded, separated_pair, terminated, tuple},
    IResult,
};

// ":" | [A-Z] | "_" | [a-z] | [#xC0-#xD6] | [#xD8-#xF6] | [#xF8-#x2FF] | [#x370-#x37D] |
// [#x37F-#x1FFF] | [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF] |
// [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
const NAME_START_CHARS: &str = ":ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
// NameStartChar | "-" | "." | [0-9] | #xB7 | [#x0300-#x036F] | [#x203F-#x2040]
// Returns whole strings matched by the given parser.
const NAME_CHARS: &str = ":ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz-.0123456789";

pub(crate) fn parse_name(source: &str) -> IResult<&str, Name> {
    map(
        recognize(
            // Runs the first parser, if succeeded then runs second, and returns the second result.
            // Note that returned ok value of `preceded()` is ignored by `recognize()`.
            preceded(
                // Parses a single character contained in the given string.
                one_of(NAME_START_CHARS),
                // Parses the longest slice consisting of the given characters
                opt(is_a(NAME_CHARS)),
            ),
        ),
        |name: &str| Name(name.to_owned()),
    )(source)
}

fn is_whitespace(ch: char) -> bool {
    ch == '\x20' || ch == '\x09' || ch == '\x0D' || ch == '\x0A'
}

pub(crate) fn take_all_whitespace(source: &str) -> IResult<&str, &str> {
    take_while1(is_whitespace)(source)
}

pub(crate) fn parse_eq(source: &str) -> IResult<&str, char> {
    delimited(
        opt(take_all_whitespace),
        char('='),
        opt(take_all_whitespace),
    )(source)
}

pub(crate) fn parse_entity_ref(source: &str) -> IResult<&str, &str> {
    recognize(delimited(char('&'), parse_name, char(';')))(source)
}

pub(crate) fn parse_char_ref(source: &str) -> IResult<&str, &str> {
    recognize(alt((
        delimited(
            tag("&#"),
            take_while(|ch: char| ch.is_ascii_digit()),
            char(';'),
        ),
        delimited(
            tag("&#x"),
            take_while(|ch: char| ch.is_ascii_hexdigit()),
            char(';'),
        ),
    )))(source)
}

pub(crate) fn parse_reference(source: &str) -> IResult<&str, &str> {
    recognize(alt((parse_entity_ref, parse_char_ref)))(source)
}

pub(crate) fn parse_attribute_value(source: &str) -> IResult<&str, AttributeValue> {
    map(
        verify(
            tuple((
                one_of("'\""),
                many0(alt((
                    // TOTO: allow the other kind of quote inside literals
                    map(none_of("<&'\""), |char| char.to_string()),
                    map(parse_reference, |str| str.to_owned()),
                ))),
                one_of("'\""),
            )),
            |(open, _, close)| open == close,
        ),
        |(_, raw_att_val, _)| AttributeValue(raw_att_val.join("")),
    )(source)
}

pub(crate) fn parse_attribute_list(source: &str) -> IResult<&str, Vec<Attribute>> {
    many0(map(
        preceded(
            take_all_whitespace,
            separated_pair(parse_name, parse_eq, parse_attribute_value),
        ),
        |(name, value)| Attribute { name, value },
    ))(source)
}

pub(crate) fn parse_start_tag(source: &str) -> IResult<&str, (Name, Vec<Attribute>)> {
    delimited(
        char('<'),
        terminated(
            pair(parse_name, parse_attribute_list),
            opt(take_all_whitespace),
        ),
        char('>'),
    )(source)
}

pub(crate) fn parse_end_tag(source: &str) -> IResult<&str, Name> {
    delimited(
        tag("</"),
        parse_name,
        pair(opt(take_all_whitespace), char('>')),
    )(source)
}

pub(crate) fn parse_char_data(source: &str) -> IResult<&str, &str> {
    recognize(verify(
        many_till(none_of(""), alt((peek(tag("]]>")), eof, peek(is_a("<&"))))),
        |s| !s.0.is_empty(),
    ))(source)
}

pub(crate) fn parse_comment(source: &str) -> IResult<&str, &str> {
    delimited(
        tag("<!--"),
        recognize(many0(alt((
            none_of("-"),
            preceded(char('-'), none_of("-")),
        )))),
        tag("-->"),
    )(source)
}

pub(crate) fn parse_cdata_section(source: &str) -> IResult<&str, &str> {
    delimited(
        tag("<![CDATA["),
        recognize(many_till(none_of(""), peek(tag("]]>")))),
        tag("]]>"),
    )(source)
}

pub(crate) fn parse_processing_instruction(source: &str) -> IResult<&str, ProcessingInstruction> {
    map(
        delimited(
            tag("<?"),
            pair(
                verify(parse_name, |name| name.0.to_lowercase() != "xml"),
                opt(preceded(
                    take_all_whitespace,
                    recognize(many_till(none_of(""), peek(tag("?>")))),
                )),
            ),
            tag("?>"),
        ),
        |(name, content)| ProcessingInstruction {
            target: name,
            content: content.unwrap_or_default().to_owned(),
        },
    )(source)
}

pub(crate) fn parse_element_content(source: &str) -> IResult<&str, Vec<Content>> {
    map(
        pair(
            opt(map(parse_char_data, |char_data| {
                Content::CharData(char_data.to_owned())
            })),
            many0(pair(
                alt((
                    map(parse_element, Content::Element),
                    map(parse_reference, |reference| {
                        Content::Reference(reference.to_owned())
                    }),
                    map(parse_cdata_section, |cdata| {
                        Content::CData(cdata.to_owned())
                    }),
                    map(parse_processing_instruction, |prcessing_instrucion| {
                        Content::PrcessingIinstrucion(prcessing_instrucion)
                    }),
                    map(parse_comment, |comment| {
                        Content::Comment(comment.to_owned())
                    }),
                )),
                opt(map(parse_char_data, |char_data| {
                    Content::CharData(char_data.to_owned())
                })),
            )),
        ),
        |(opt_char_data, vec): (Option<Content>, Vec<(Content, Option<Content>)>)| {
            let mut v = vec![];
            if let Some(opt_char_data) = opt_char_data {
                v.push(opt_char_data);
            }
            for (con, opt_char_data) in vec {
                v.push(con);
                if let Some(opt_char_data) = opt_char_data {
                    v.push(opt_char_data);
                }
            }
            v
        },
    )(source)
}

pub(crate) fn parse_element_not_empty(source: &str) -> IResult<&str, Element> {
    map(
        verify(
            tuple((parse_start_tag, parse_element_content, parse_end_tag)),
            |((start_name, _), _, end_name)| start_name == end_name,
        ),
        |((name, attributes), content, _)| Element {
            name,
            attributes,
            content,
        },
    )(source)
}

pub(crate) fn parse_element_empty(source: &str) -> IResult<&str, Element> {
    map(
        delimited(
            char('<'),
            terminated(
                pair(parse_name, parse_attribute_list),
                opt(take_all_whitespace),
            ),
            tag("/>"),
        ),
        |(name, attributes)| Element {
            name,
            attributes,
            content: vec![],
        },
    )(source)
}

pub(crate) fn parse_element(source: &str) -> IResult<&str, Element> {
    alt((parse_element_not_empty, parse_element_empty))(source)
}

pub(crate) fn parse_misc(source: &str) -> IResult<&str, Misc> {
    alt((
        map(parse_comment, |s| Misc::Comment(s.to_owned())),
        map(parse_processing_instruction, Misc::ProcessingInstruction),
        map(take_all_whitespace, |_| Misc::Space),
    ))(source)
}

//ciao amore <3
pub(crate) fn parse_version(source: &str) -> IResult<&str, &str> {
    preceded(
        tuple((take_all_whitespace, tag("version"), parse_eq)),
        alt((
            delimited(
                char('\''),
                recognize(tuple((tag("1."), digit1))),
                char('\''),
            ),
            delimited(char('"'), recognize(tuple((tag("1."), digit1))), char('"')),
        )),
    )(source)
}

pub(crate) fn parse_encoding(source: &str) -> IResult<&str, &str> {
    preceded(
        tuple((take_all_whitespace, tag("encoding"), parse_eq)),
        alt((
            delimited(
                char('\''),
                recognize(preceded(
                    one_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTYUVWXYZ"),
                    is_a("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTYUVWXYZ0123456789._-"),
                )),
                char('\''),
            ),
            delimited(
                char('"'),
                recognize(preceded(
                    one_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTYUVWXYZ"),
                    is_a("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTYUVWXYZ0123456789._-"),
                )),
                char('"'),
            ),
        )),
    )(source)
}

pub(crate) fn parse_standalone(source: &str) -> IResult<&str, bool> {
    preceded(
        tuple((take_all_whitespace, tag("standalone"), parse_eq)),
        alt((
            delimited(
                char('\''),
                alt((map(tag("yes"), |_| true), map(tag("no"), |_| false))),
                char('\''),
            ),
            delimited(
                char('"'),
                alt((map(tag("yes"), |_| true), map(tag("no"), |_| false))),
                char('"'),
            ),
        )),
    )(source)
}

pub(crate) fn parse_xml_declaration(source: &str) -> IResult<&str, XMLDeclaration> {
    map(
        delimited(
            tag("<?xml"),
            terminated(
                tuple((parse_version, opt(parse_encoding), opt(parse_standalone))),
                opt(take_all_whitespace),
            ),
            tag("?>"),
        ),
        |(version, encoding, standalone)| XMLDeclaration {
            version: version.to_owned(),
            encoding: encoding.map(str::to_owned),
            standalone,
        },
    )(source)
}

pub(crate) fn parse_system_literal(source: &str) -> IResult<&str, SystemLiteral> {
    map(
        alt((
            delimited(char('"'), alt((is_not("\""), success(""))), char('"')),
            delimited(char('\''), alt((is_not("'"), success(""))), char('\'')),
        )),
        |str| SystemLiteral(str.to_owned()),
    )(source)
}

pub(crate) fn parse_pubid_literal(source: &str) -> IResult<&str, PubIDLiteral> {
    map(
        alt((
            delimited(char('"'), alt((is_a("\x20\x0A\x0DabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-'()+,./:=?;!*#@$_%"), success(""))), char('"')),
            delimited(char('\''), alt((is_a("\x20\x0A\x0DabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-'()+,./:=?;!*#@$_%"), success(""))), char('\'')),
        )),
        |str| PubIDLiteral(str.to_owned()),
    )(source)
}

pub(crate) fn parse_external_id(source: &str) -> IResult<&str, ExternalID> {
    alt((
        map(
            preceded(
                preceded(tag("SYSTEM"), take_all_whitespace),
                parse_system_literal,
            ),
            |identifier| ExternalID::System { identifier },
        ),
        map(
            tuple((
                preceded(
                    preceded(tag("PUBLIC"), take_all_whitespace),
                    parse_pubid_literal,
                ),
                preceded(take_all_whitespace, parse_system_literal),
            )),
            |(pub_literal, identifier)| ExternalID::Public {
                pub_literal,
                identifier,
            },
        ),
    ))(source)
}

pub(crate) fn parse_content_spec_mixed(source: &str) -> IResult<&str, Vec<Mixed>> {
    alt((
        map(
            delimited(
                pair(char('('), opt(take_all_whitespace)),
                pair(
                    tag("#PCDATA"),
                    many0(preceded(
                        delimited(
                            opt(take_all_whitespace),
                            char('|'),
                            opt(take_all_whitespace),
                        ),
                        parse_name,
                    )),
                ),
                pair(opt(take_all_whitespace), tag(")*")),
            ),
            |(_, vec)| {
                let mut v = vec![Mixed::PCData];
                v.extend(vec.into_iter().map(Mixed::Name));
                v
            },
        ),
        map(
            delimited(
                pair(char('('), opt(take_all_whitespace)),
                tag("#PCDATA"),
                pair(opt(take_all_whitespace), char(')')),
            ),
            |_| vec![Mixed::PCData],
        ),
    ))(source)
}

pub(crate) fn parse_content_spec_content_particle(source: &str) -> IResult<&str, ContentParticle> {
    map(
        pair(
            alt((
                map(parse_name, NameOrChoiceOrSequence::Name),
                map(
                    parse_content_spec_children_choice,
                    NameOrChoiceOrSequence::Choice,
                ),
                map(
                    parse_content_spec_children_sequence,
                    NameOrChoiceOrSequence::Sequence,
                ),
            )),
            opt(alt((char('?'), char('*'), char('+')))),
        ),
        |(n_c_s, char)| match char {
            Some('?') => ContentParticle::Optional(n_c_s),
            Some('*') => ContentParticle::Many0(n_c_s),
            Some('+') => ContentParticle::Many1(n_c_s),
            None => ContentParticle::One(n_c_s),
            Some(_) => unreachable!(),
        },
    )(source)
}

pub(crate) fn parse_content_spec_children_choice(source: &str) -> IResult<&str, Choice> {
    map(
        delimited(
            pair(char('('), opt(take_all_whitespace)),
            verify(
                separated_list1(
                    delimited(
                        opt(take_all_whitespace),
                        char('|'),
                        opt(take_all_whitespace),
                    ),
                    parse_content_spec_content_particle,
                ),
                |v: &Vec<_>| v.len() >= 2,
            ),
            pair(opt(take_all_whitespace), char(')')),
        ),
        Choice,
    )(source)
}

pub(crate) fn parse_content_spec_children_sequence(source: &str) -> IResult<&str, Sequence> {
    map(
        delimited(
            pair(char('('), opt(take_all_whitespace)),
            separated_list1(
                delimited(
                    opt(take_all_whitespace),
                    char(','),
                    opt(take_all_whitespace),
                ),
                parse_content_spec_content_particle,
            ),
            pair(opt(take_all_whitespace), char(')')),
        ),
        Sequence,
    )(source)
}

pub(crate) fn parse_content_spec_children(source: &str) -> IResult<&str, Children> {
    map(
        pair(
            alt((
                map(parse_content_spec_children_choice, ChoiceOrSequence::Choice),
                map(
                    parse_content_spec_children_sequence,
                    ChoiceOrSequence::Sequence,
                ),
            )),
            opt(alt((char('?'), char('*'), char('+')))),
        ),
        |(c_s, char)| match char {
            Some('?') => Children::Optional(c_s),
            Some('*') => Children::Many0(c_s),
            Some('+') => Children::Many1(c_s),
            None => Children::One(c_s),
            Some(_) => unreachable!(),
        },
    )(source)
}

pub(crate) fn parse_content_spec(source: &str) -> IResult<&str, ContentSpec> {
    alt((
        map(tag("EMPTY"), |_| ContentSpec::Empty),
        map(tag("ANY"), |_| ContentSpec::Any),
        map(parse_content_spec_mixed, ContentSpec::Mixed),
        map(parse_content_spec_children, ContentSpec::Children),
    ))(source)
}

pub(crate) fn parse_element_declaration(source: &str) -> IResult<&str, ElementDeclaration> {
    map(
        delimited(
            tag("<!ELEMENT"),
            terminated(
                pair(
                    preceded(take_all_whitespace, parse_name),
                    preceded(take_all_whitespace, parse_content_spec),
                ),
                opt(take_all_whitespace),
            ),
            char('>'),
        ),
        |(name, content_spec)| ElementDeclaration { name, content_spec },
    )(source)
}

pub(crate) fn parse_tokenized_type(source: &str) -> IResult<&str, TokenizedType> {
    alt((
        map(tag("ID"), |_| TokenizedType::Id),
        map(tag("IDREF"), |_| TokenizedType::IdRef),
        map(tag("IDREFS"), |_| TokenizedType::IdRefs),
        map(tag("ENTITY"), |_| TokenizedType::Entity),
        map(tag("ENTITIES"), |_| TokenizedType::Entities),
        map(tag("NMTOKEN"), |_| TokenizedType::NMToken),
        map(tag("NMTOKENS"), |_| TokenizedType::NMTokens),
    ))(source)
}

pub(crate) fn parse_notation_type(source: &str) -> IResult<&str, EnumeratedType> {
    map(
        preceded(
            pair(tag("NOTATION"), take_all_whitespace),
            delimited(
                pair(char('('), opt(take_all_whitespace)),
                separated_list1(
                    delimited(
                        opt(take_all_whitespace),
                        char('|'),
                        opt(take_all_whitespace),
                    ),
                    parse_name,
                ),
                pair(opt(take_all_whitespace), char(')')),
            ),
        ),
        EnumeratedType::NotationType,
    )(source)
}

pub(crate) fn parse_name_token(source: &str) -> IResult<&str, NameToken> {
    map(is_a(NAME_CHARS), |s: &str| NameToken(s.to_owned()))(source)
}

pub(crate) fn parse_enumeration(source: &str) -> IResult<&str, EnumeratedType> {
    map(
        delimited(
            pair(char('('), opt(take_all_whitespace)),
            separated_list1(
                delimited(
                    opt(take_all_whitespace),
                    char('|'),
                    opt(take_all_whitespace),
                ),
                parse_name_token,
            ),
            pair(opt(take_all_whitespace), char(')')),
        ),
        EnumeratedType::Enumeration,
    )(source)
}

pub(crate) fn parse_enumerated_type(source: &str) -> IResult<&str, EnumeratedType> {
    alt((parse_notation_type, parse_enumeration))(source)
}

pub(crate) fn parse_attribute_type(source: &str) -> IResult<&str, AttributeType> {
    alt((
        map(tag("CDATA"), |_| AttributeType::StringType),
        map(parse_tokenized_type, AttributeType::TokenizedType),
        map(parse_enumerated_type, AttributeType::EnumeratedType),
    ))(source)
}

pub(crate) fn parse_default_declaration(source: &str) -> IResult<&str, DefaultDeclaration> {
    alt((
        map(tag("#REQUIRED"), |_| DefaultDeclaration::Required),
        map(tag("#IMPLIED"), |_| DefaultDeclaration::Implied),
        map(
            preceded(
                opt(pair(tag("#FIXED"), take_all_whitespace)),
                parse_attribute_value,
            ),
            DefaultDeclaration::Fixed,
        ),
    ))(source)
}

pub(crate) fn parse_attribute_definition(source: &str) -> IResult<&str, AttributeDefinition> {
    map(
        tuple((
            preceded(take_all_whitespace, parse_name),
            preceded(take_all_whitespace, parse_attribute_type),
            preceded(take_all_whitespace, parse_default_declaration),
        )),
        |(name, attribute_type, default_declaration)| AttributeDefinition {
            name,
            attribute_type,
            default_declaration,
        },
    )(source)
}

pub(crate) fn parse_attribute_list_declaration(
    source: &str,
) -> IResult<&str, AttributeListDeclaration> {
    map(
        delimited(
            tag("<!ATTLIST"),
            pair(
                preceded(take_all_whitespace, parse_name),
                many0(parse_attribute_definition),
            ),
            preceded(opt(take_all_whitespace), char('>')),
        ),
        |(name, attribute_definitions)| AttributeListDeclaration {
            name,
            attribute_definitions,
        },
    )(source)
}

pub(crate) fn parse_entity_value(source: &str) -> IResult<&str, EntityValue> {
    map(
        alt((
            delimited(
                char('\''),
                recognize(many0(alt((
                    is_not("%&'"),
                    parse_pe_reference,
                    parse_reference,
                )))),
                char('\''),
            ),
            delimited(
                char('"'),
                recognize(many0(alt((
                    is_not("%&\""),
                    parse_pe_reference,
                    parse_reference,
                )))),
                char('"'),
            ),
        )),
        |s| EntityValue(s.to_string()),
    )(source)
}

pub(crate) fn parse_ndata_decl(source: &str) -> IResult<&str, NDataDeclaration> {
    map(
        preceded(
            tuple((take_all_whitespace, tag("NDATA"), take_all_whitespace)),
            parse_name,
        ),
        NDataDeclaration,
    )(source)
}

pub(crate) fn parse_entity_def(source: &str) -> IResult<&str, EntityDef> {
    alt((
        map(parse_entity_value, EntityDef::EntityValue),
        map(
            pair(parse_external_id, opt(parse_ndata_decl)),
            |(id, data)| EntityDef::External(id, data),
        ),
    ))(source)
}

pub(crate) fn parse_ge_declaration(source: &str) -> IResult<&str, GEDeclaration> {
    map(
        delimited(
            tag("<!ENTITY"),
            pair(
                preceded(take_all_whitespace, parse_name),
                preceded(take_all_whitespace, parse_entity_def),
            ),
            pair(opt(take_all_whitespace), char('>')),
        ),
        |(name, entity_def)| GEDeclaration { name, entity_def },
    )(source)
}

pub(crate) fn parse_pe_def(source: &str) -> IResult<&str, PEDef> {
    alt((
        map(parse_entity_value, PEDef::EntityValue),
        map(parse_external_id, PEDef::External),
    ))(source)
}

pub(crate) fn parse_pe_declaration(source: &str) -> IResult<&str, PEDeclaration> {
    map(
        delimited(
            tuple((tag("<!ENTITY"), take_all_whitespace, char('%'))),
            pair(
                preceded(take_all_whitespace, parse_name),
                preceded(take_all_whitespace, parse_pe_def),
            ),
            pair(opt(take_all_whitespace), char('>')),
        ),
        |(name, pe_def)| PEDeclaration { name, pe_def },
    )(source)
}

pub(crate) fn parse_entity_declaration(source: &str) -> IResult<&str, EntityDeclaration> {
    alt((
        map(parse_ge_declaration, EntityDeclaration::GEDeclaration),
        map(parse_pe_declaration, EntityDeclaration::PEDeclaration),
    ))(source)
}

pub(crate) fn parse_public_id(source: &str) -> IResult<&str, PublicID> {
    map(
        preceded(
            pair(tag("PUBLIC"), take_all_whitespace),
            parse_pubid_literal,
        ),
        PublicID,
    )(source)
}

pub(crate) fn parse_id(source: &str) -> IResult<&str, Id> {
    alt((
        map(parse_external_id, Id::ExternalID),
        map(parse_public_id, Id::PublicID),
    ))(source)
}

pub(crate) fn parse_notation_declaration(source: &str) -> IResult<&str, NotationDeclaration> {
    map(
        delimited(
            tag("<!NOTATION"),
            pair(
                preceded(take_all_whitespace, parse_name),
                preceded(take_all_whitespace, parse_id),
            ),
            pair(opt(take_all_whitespace), char('>')),
        ),
        |(name, id)| NotationDeclaration { name, id },
    )(source)
}

pub(crate) fn parse_markup_declaration(source: &str) -> IResult<&str, MarkupDeclaration> {
    alt((
        map(
            parse_element_declaration,
            MarkupDeclaration::ElementDeclaration,
        ),
        map(
            parse_attribute_list_declaration,
            MarkupDeclaration::AttributeListDeclaration,
        ),
        map(
            parse_entity_declaration,
            MarkupDeclaration::EntityDeclaration,
        ),
        map(
            parse_notation_declaration,
            MarkupDeclaration::NotationDeclaration,
        ),
        map(
            parse_processing_instruction,
            MarkupDeclaration::ProcessingInstruction,
        ),
        map(parse_comment, |s| MarkupDeclaration::Comment(s.to_owned())),
    ))(source)
}

pub(crate) fn parse_pe_reference(source: &str) -> IResult<&str, &str> {
    recognize(delimited(char('%'), parse_name, char(';')))(source)
}

pub(crate) fn parse_delaration_separator(source: &str) -> IResult<&str, &str> {
    recognize(alt((parse_pe_reference, take_all_whitespace)))(source)
}

pub(crate) fn parse_int_subset(source: &str) -> IResult<&str, Vec<MarkupDeclaration>> {
    map(
        terminated(
            delimited(
                char('['),
                many0(alt((
                    map(parse_markup_declaration, Some),
                    map(parse_delaration_separator, |_| None),
                ))),
                char(']'),
            ),
            opt(take_all_whitespace),
        ),
        |vec| vec.into_iter().flatten().collect(),
    )(source)
}

pub(crate) fn parse_doctype(source: &str) -> IResult<&str, Doctype> {
    map(
        delimited(
            tag("<!DOCTYPE"),
            tuple((
                preceded(take_all_whitespace, parse_name),
                opt(preceded(take_all_whitespace, parse_external_id)),
                preceded(opt(take_all_whitespace), opt(parse_int_subset)),
            )),
            char('>'),
        ),
        |(name, external_id, int_subset)| Doctype {
            name,
            external_id,
            int_subset,
        },
    )(source)
}

pub(crate) fn parse_prolog(source: &str) -> IResult<&str, Prolog> {
    map(
        tuple((
            opt(parse_xml_declaration),
            many0(parse_misc),
            opt(parse_doctype),
            many0(parse_misc),
        )),
        |(xml_declaration, _, doctype, _)| Prolog {
            xml_declaration,
            doctype,
        },
    )(source)
}

pub fn parse(source: &str) -> IResult<&str, Document> {
    map(
        tuple((parse_prolog, parse_element, many0(parse_misc))),
        |(prolog, root, _miscs)| Document { prolog, root },
    )(source)
}
